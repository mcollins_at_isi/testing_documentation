# Setting up an Experiment on a Merge Testbed

This document walks through setting up an experiment on a Merge
testbed using a nontrivial network topology.  An experimenter using a
Merge testbed can quickly and consistently instantiate an accurate
network emulation.  However, developing a quality test requires
provisioning on the instantiated host and understanding how Merge
configuresw network infrastructure.  

On completion of this document, the reader will have set up an
experimental framework from scratch using a test topology which
consists of a client that communicates with one or more servers by
passing through a gateway.  By working through the steps shown here,
the reader will understand how to create a topology, how to realize
it, materialize it and provision it.

The remainder of this document is structured as follows: after this
introduction is a high-level description of the phases of the
experimentation process, a list of terms of art for the merge testbed,
and a description of the test network.  Following this, each phase is
described in depth with step-by-step instructions.

## Expected Skills and References

The target audience for this document is a graduate student who has
some basic system administration experience.  In particular, the
reader should be able to:

* Set up ssh connection using a public key and edit an ssh configuration file
* Be familiar with shell scripting, and capable of working in a fresh, minimal, non-graphical Linux installation
* Have familiarity with basic traffic analysis tools: ```netstat```, ```ifconfig```,```ping```,```traceroute``` and ```nc```.
* Be able to install packages using ```apt-get``` and understand how to use ```sudo```

In addition, the following tutorials will be useful:

* Learning how Linux routing works using ```ip route```; the tutorial
  at
  https://www.linuxjournal.com/content/linux-advanced-routing-tutorial
  is a reasonable introduction.
* Understanding how to use ansible for bulk provisioning. The standard
  ansible tutorial is available at https://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html


## The Experimentation Process

This section provides a high-level overview of the experimentation
process.  We assume that the reader has already defined the experiment
-- that is, defined observables, criteria for success and failure, and
has a rough model of the components making up the experiment, their
provisioning, and how they are connected to each other.  The
experimentation process consists of the following broad steps:

1. Design.  During the design step, the experimenter creates a
   topology for materialization; this topology is specified in a XIR
   file for the realization step.
2. Realization.  During the realization step, the experimenter reserves the resources specified durign the design step.  
3. Materialization.  During the materialization step, the testbed
   creates virtual hosts from the resource reserved during
   realization.
4. Provisioning.  During the provisioning step, the experimenter configures the virtual hosts created during materialization. 
5. Execution.  During the execution step, the experimenter runs their experiment and collects results. 
	

## Notes on Terminology

* Materialization: The process of using the resources reserved during realization. 
* Realization: The process of taking a _topology_ and reserving the resources which will be _materialized_ on the testbed.
* Infranet: The infrastructure network; this network provides routing and DNS support as a number of other infrastructure support functions. All hosts at least homed to the Infranet.
* XDC: Experiment development container.  A virtual machine providing access to a particular experiment.
* XIR: Experiment Model Intermediate Representation.  A json-based format for describing the hosts and network connections which comprise a test network.

## Test network

The test network shown in the visualization below is a simple network
intended to test different behaviors in accessing a webserver.  The
network consists of three subnetworks:
	* Instrumentation: Consisting of the server\_inst, server\_uninst, inst\_gw and inst nodes.  This network allocat
	* A small bridge network consisting of the inst\_gw and cr nodes.
	* The client network, consisting of the cr and client nodes.
	
### Step 0: Connecting to an XDC

In this section, we will walk through the necessary configuration
steps to access the XDC via an ssh command line. While SSH
configuration is more complex than using the web interface, it is more
flexible, an issue particular when you are transferring complex
configuration files to the XDC.  Because the XDC's are not directly
exposed to the Internet, ssh access is done using a jump host as the
gateway. This means that all ssh connections are forwarded -- first
you connect to the jump host, then you connect to the XDC. 

Each XDC is automatically assigned a name of the form <local
name>-<experiment>-<project>.  For example, if I have a project
'mpcollins', and then create an experiment 'shorttermtopotest', and
then create an XDC 'activexdc', the name of the XDC is
```activexdc-shorttermtopotest-mpcollins``` .  The jump host will
manage name resolution, so the first task is to ensure we get to the jump host 

At this point, you should have uploaded a public key to the merge interface.  This public key will be used to access the XDC, the jump host and any instantiated hosts on the testbed.  We recommend setting up all argument in your ssh config file, as the ssh command line client arguements are generally not passed to the jump host. 

```
#
# Template for SSH access to mergetb XDC, to use:
# Change PATH_TO_SSH to point to your .ssh directory -- where you keep
# keys and config
# Change USERNAME to the user name you use on the testbed

Host jumpc.mergetb.io
User USERNAME
ForwardAgent yes
IdentityFile /PATH_TO_SSH/IDENTITYFILE

Host *-USERNAME
Hostname %h
ProxyJump jumpc.mergetb.io:2202
User USERNAME
IdentityFile /PATH_TO_SSH/IDENTITYFILE
```

If this is properly configured, then you should be asked to enter in your passphrase twice -- once to get onto the jump host, once to get onto the XDC.


### Step 1: Design and Realization

During the design and realization step, the experimenter develops a
network design and verifies that the testbed can realize that design.

### Step 2: Materialization

### Step 3: Provisioning

Once a network is materialized, the experimenter needs to configure
the individual assets on the network.  Materialized hosts are in a
barebones configuration which ensures that the hosts can communicate
with each other and the infranet, but does not provide internal
routing or other services.  

#### Step 3.2: Verifying asset inventory

At this point, you will have a materialized network and a
specification, the next phase of the work involves verifying that all
assets are present, and as specified, correctly configured.  In this
section, we will walk through dynamically verifying your assets from
the XDC.  This process involves two steps: creating a list of all the
host names as recognized by the database, then collecting an inventory
of addresses and verifying that they are correctly assigned to the
hosts.

```mergetb``` provides a complete suite of commands for examining
individual testbed components. In this case, using the ```mergetb
show``` command, we can get a complete list of components.  ```mergetb
show```, by default will provide text output.  For example:
```
root@activexdc:/xp# mergetb show realization shorttermtopotest newtest
NODE             RESOURCE    SITE
client           m334        site.dcomptb.net
cr               m305        site.dcomptb.net
inst             m306        site.dcomptb.net
inst_gw          m335        site.dcomptb.net
server_inst      m295        site.dcomptb.net
server_uninst    m288        site.dcomptb.net
```

In this example, ```mergetb show realization``` shows the hosts in the test realization -- note that the columns show the name you can access the host by (the NODE value), the internal ID of the resource assigned to that (this will change in each materialization) and the site the node is running on.  This output is not suitable for direct conversion to files; for machine readable output, use the JSON output instead, the following snippet will dump all the names:
```
root@activexdc:/xp# mergetb show realization shorttermtopotest newtest -j | jq '.nodes | to_entries[].key' | tr -d '"'
client
cr
inst
inst_gw
server_inst
server_uninst
```

#### Setting up bulk ssh access

After creating the login file, the next step should be to set up ssh
access to all hosts.  During the materialization process, the testbed
creates a user account on each host, with sudo access, using your
account name.  For example, in the tests for this demonstration, I am
logged onto merge using the account mpcollins.  The ssh key that you
uploaded when creating the project is copied to each of those hosts,
so you should be able to log on automatically using the id and the
public key.

Because you are going to be repeatedly accessing hosts via ```ssh```;
setting up an ```ssh-agent``` and storing your key will be more
effective.  At the start of provisioning, I run the following
processes:

```
root@activexdc:/xp# ssh-agent /bin/bash
root@activexdc:/xp# ssh-add id_mergetb
Enter passphrase for id_mergetb:
for i in `cat hosts`; do ssh mpcollins@${i} -o StrictHostKeyChecking=accept-new 'echo logged onto `hostname`'
> done

Warning: Permanently added 'client,172.30.0.12' (ECDSA) to the list of known hosts.
logged onto client
Warning: Permanently added 'cr,172.30.0.10' (ECDSA) to the list of known hosts.
logged onto cr
Warning: Permanently added 'inst,172.30.0.13' (ECDSA) to the list of known hosts.
logged onto inst
Warning: Permanently added 'inst_gw,172.30.0.11' (ECDSA) to the list of known hosts.
logged onto instgw
Warning: Permanently added 'server_inst,172.30.0.15' (ECDSA) to the list of known hosts.
logged onto serverinst
Warning: Permanently added 'server_uninst,172.30.0.14' (ECDSA) to the list of known hosts.
```

This approach uses the recently added ```StrictHostKeyChecking=accept-new```
ssh option to automatically accept all new host keys, skipping the
need for user input while updating the ```known_hosts``` file. The
echo command is a no-op; it exists simply to give ssh something to do
which succeeds on a real connection and doesn't drop into the
shell during this setup.

At this point, you can communicate with every host on the network from
the xdc via ssh without any additional prompting.  Commands throughout
the remainder of this walkthrough will be executed non-interactively
via ssh.

The first thing to do should be to dump all the interfaces and compare
them against specification.  A simple command line script to do so is
shown below.

```
root@activexdc:/xp# IFS=$'\n'; for j in `cat hosts`; do for i in `ssh mpcollins@${j} "ip addr show | grepinet"`;do printf "%-20s %s\n" ${j} ${i}; done; done; unset IFS
client                   inet 127.0.0.1/8 scope host lo
client                   inet6 ::1/128 scope host
client                   inet 172.30.0.12/16 brd 172.30.255.255 scope global dynamic eth0
client                   inet6 fe80::208:a2ff:fe0d:de7e/64 scope link
client                   inet 10.0.2.2/24 brd 10.0.2.255 scope global eth1
client                   inet6 fe80::208:a2ff:fe0d:de7f/64 scope link
cr                       inet 127.0.0.1/8 scope host lo
cr                       inet6 ::1/128 scope host
cr                       inet 172.30.0.10/16 brd 172.30.255.255 scope global dynamic eth0
cr                       inet6 fe80::208:a2ff:fe0d:de4a/64 scope link
cr                       inet6 fe80::208:a2ff:fe0d:de4b/64 scope link
cr                       inet 10.0.1.1/24 brd 10.0.1.255 scope global eth1.68
cr                       inet6 fe80::208:a2ff:fe0d:de4b/64 scope link
cr                       inet 10.0.2.3/24 brd 10.0.2.255 scope global eth1.67
cr                       inet6 fe80::208:a2ff:fe0d:de4b/64 scope link
inst                     inet 127.0.0.1/8 scope host lo
inst                     inet6 ::1/128 scope host
inst                     inet 172.30.0.13/16 brd 172.30.255.255 scope global dynamic eth0
inst                     inet6 fe80::208:a2ff:fe0d:dca8/64 scope link
inst                     inet 10.0.0.6/24 brd 10.0.0.255 scope global eth1
inst                     inet6 fe80::208:a2ff:fe0d:dca9/64 scope link
inst_gw                  inet 127.0.0.1/8 scope host lo
inst_gw                  inet6 ::1/128 scope host
inst_gw                  inet 172.30.0.11/16 brd 172.30.255.255 scope global dynamic eth0
inst_gw                  inet6 fe80::208:a2ff:fe0d:dcbc/64 scope link
inst_gw                  inet6 fe80::208:a2ff:fe0d:dcbd/64 scope link
inst_gw                  inet 10.0.0.7/24 brd 10.0.0.255 scope global eth1.66
inst_gw                  inet6 fe80::208:a2ff:fe0d:dcbd/64 scope link
inst_gw                  inet 10.0.1.2/24 brd 10.0.1.255 scope global eth1.68
inst_gw                  inet6 fe80::208:a2ff:fe0d:dcbd/64 scope link
server_inst              inet 127.0.0.1/8 scope host lo
server_inst              inet6 ::1/128 scope host
server_inst              inet 172.30.0.15/16 brd 172.30.255.255 scope global dynamic eth0
server_inst              inet6 fe80::208:a2ff:fe0d:dc50/64 scope link
server_inst              inet 10.0.0.4/24 brd 10.0.0.255 scope global eth1
server_inst              inet6 fe80::208:a2ff:fe0d:dc51/64 scope link
server_uninst            inet 127.0.0.1/8 scope host lo
server_uninst            inet6 ::1/128 scope host
server_uninst            inet 172.30.0.14/16 brd 172.30.255.255 scope global dynamic eth0
server_uninst            inet6 fe80::208:a2ff:fe0d:df68/64 scope link
server_uninst            inet 10.0.0.5/24 brd 10.0.0.255 scope global eth1
server_uninst            inet6 fe80::208:a2ff:fe0d:df69/64 scope link
```

The result of the various ```ip show``` commands is the listing of all
IP addresses and associated interfaces for each materialized host.
Note that every host has _at least_ one interface to the Infranet
(172.30.0.0/16) as well as the interface in the specification.

As with the host names, it makes sense to convert this information
into a dynamically updated database.  For example, in this network, we
know all the addresses we care about are in the 10.0.0.0/8 subnet, so
we will just grep for 10.0.0.0/8 addresses as follows:

```
root@activexdc:/xp# IFS=$'\n'; for j in `cat hosts`; do for i in `ssh mpcollins@${j} "ip addr show | grepinet | grep '10\.0'"`;do printf "%-20s %s\n" ${j} ${i} >> x.txt; done; done; unset IFS
root@activexdc:/xp# cat x.txt | tr -s ' ' | cut -d ' ' -f 1,3 > ips.txt
root@activexdc:/xp# cat ips.txt
client 10.0.2.2/24
cr 10.0.1.1/24
cr 10.0.2.3/24
inst 10.0.0.6/24
inst_gw 10.0.0.7/24
inst_gw 10.0.1.2/24
server_inst 10.0.0.4/24
server_uninst 10.0.0.5/24
```

At this point, you have a list of hosts (hosts.txt) and IP addresses (ips.txt); 
this is the list of all hosts which are accessible directly from the xdc.  In addition, you have configured ssh to connect to each host without user prompting.  

#### Step 3.2: Setting up the network

You will not configure the test network so that communications are
done within the network as opposed to piggybacking off of the
infranet.  The infranet provides the following network services:
* A common DNS server
* Routing through the infranet to all other assets

The attached figure shows how the sample network is configured during
a test realization.  Note as discussed above that all hosts on the
network are _at least_ dual homed: all hosts have an IP address on the
172.30.0.0/16 network block (the Infranet) as well as any additional
addresses they were configured with during setup.  By default, hosts
will communicate through the infranet and the Infranet's DNS server
uses the hosts Infranet (_i.e._ 172.30.0.0/16) addresses.  At this
point, hosts cannot route.

We recommend conducting network setup in two phases: 

1. Set up packet forwarding on middleboxes
2. Configure routing directly

We note in passing that these two phases assume that you are using a
very simple configuration without the need of BGP, MPLS or any more
complex routing.  Tests and documentation for those will be
forthcoming.

To support network analysis, we have a simple quality of life script called
```allping```.  allping sequentially ```ssh's``` each named host and pings all requested IP addresses from that host.  Sample output is shown below: 

```
root@activexdc:/xp# bash allping.bash mpcollins hosts ip_only.txt
client                      10.0.2.2 2 packets transmitted, 2 received, 0% packet loss, time 6ms
client                      10.0.1.1 2 packets transmitted, 2 received, 0% packet loss, time 23ms
client                      10.0.2.3 2 packets transmitted, 2 received, 0% packet loss, time 3ms
client                      10.0.0.6 2 packets transmitted, 2 received, 0% packet loss, time 2ms
client                      10.0.0.7 2 packets transmitted, 2 received, 0% packet loss, time 3ms
client                      10.0.1.2 2 packets transmitted, 2 received, 0% packet loss, time 3ms
```

```allping``` will need several seconds per attempt, so on large
networks this command will take some time to run.  Reducing the set of
ip addresses down to a single address per subnet will significantly
improve performance.

##### Packet Forwarding

All devices are materialized without packet forwarding.  Any device
you intend to use as a middlebox, proxy, router or firewall will
consequently need forwarding turned on.  In the test topology, those
devices are ```inst_gw``` and ```cr```.  Packet forwarding is
controlled by the ```net.ipv4.ip_forward``` kernel option.  To turn on
forwarding on a host:

```
ssh mpcollins@inst_gw `sudo /sbin/sysctl -w net.ipv4.ip_forward=1`
```

##### Routing

We will now discuss how to set up routing using the ```ip route```
command.  Note that using ```ip route``` will set up routes on that
host until the host is rebooted; if you want to set up permanent
routes you can manually edit the relevant files.  This section is
scoped specifically to configuration using ```ip route``` and does not
cover dynamic routing such as via BGP.

```ip route``` is used to manipulate a host's routing table at the
command line.  Routes are expressed via a CIDR block and a
gateway. For example, a properly configured version of the ```inst```
host in the example looks like the following:

```
root@activexdc:/xp# ssh mpcollins@inst 'ip route show'
default via 172.30.0.1 dev eth0 proto dhcp src 172.30.0.15 metric 1024
10.0.0.0/24 dev eth1 proto kernel scope link src 10.0.0.6
10.0.1.0/24 via 10.0.0.7 dev eth1
10.0.2.0/24 via 10.0.0.7 dev eth1
172.30.0.0/16 dev eth0 proto kernel scope link src 172.30.0.15
172.30.0.1 dev eth0 proto dhcp scope link src 172.30.0.15 metric 1024
```

Note that each route consists of a CIDR block and optionally a device
or a via statement.  Device lines specify that the asset is physically
connected to the CIDR block in question and send packets directly
there.  For example, if you consult the digram, you will see that the
```inst``` host is directly connected to other assets in the
10.0.0.0/24 CIDR block, but to reach ```client``` it needs access to
the 10.0.2.0/24 CIDR block -- this is done by traveling through the
10.0.0.7 address on ```inst_gw```.  If we then look at the routes on
```inst_gw```, we will see:

```
root@activexdc:/xp# ssh mpcollins@inst_gw 'ip route show'
default via 172.30.0.1 dev eth0 proto dhcp src 172.30.0.11 metric 1024
10.0.0.0/24 dev eth1.43 proto kernel scope link src 10.0.0.7
10.0.1.0/24 dev eth1.45 proto kernel scope link src 10.0.1.2
10.0.2.0/24 via 10.0.1.1 dev eth1.45
172.30.0.0/16 dev eth0 proto kernel scope link src 172.30.0.11
172.30.0.1 dev eth0 proto dhcp scope link src 172.30.0.11 metric 1024
```

As this shows, the ```inst_gw``` communicates the 10.0.2.0/24 via the
gateway address 10.0.1.1; if you consult the network diagram, you will
see that 10.0.1.1 is one of the addresses on the multihomed ```cr```
node.  In summary, to get to ```client```, ```inst``` sends a packet
with a destination address of 10.0.2.2 (```client```'s address) to
10.0.0.7 (```inst_gw```'s address).  ```inst_gw``` then sends the
packet to 10.0.1.1 (where it is plugged to ```cr```), which in turn
sends the packet to ```client```.  

Configuring routing requires specifying _all_ the necessary paths
through the relevant nodes.  Since these paths are decomposable into
individual hops, the process of determining the relevant paths
involves developing a spanning tree.  For example, in the test
network, the relevant routes are:

- Anything in the 10.0.0.0/24 CIDR block communicates with 10.0.2.0/24 via 10.0.0.7
- Anything in the 10.0.2.0/24 CIDR block communications with 10.0.0.0/24 via 10.0.2.3
- ```inst_gw``` communicates with ```cr``` via 10.0.1.2
- ```cr``` communicates with ```inst_gw``` via 10.0.2.2

This yields the following specific configurations for the test network:
```
Host           Subnet      Gateway
cr             10.0.0.0/24 10.0.1.2
client         10.0.0.0/24 10.0.2.3
client         10.0.1.0/24 10.0.2.3
inst_gw        10.0.2.0/24 10.0.1.1
inst           10.0.2.0/24 10.0.0.7
inst           10.0.1.0/24 10.0.0.7
server_inst    10.0.2.0/24 10.0.0.7
server_inst    10.0.1.0/24 10.0.0.7
server_uninst  10.0.2.0/24 10.0.0.7
server_uninst  10.0.1.0/24 10.0.0.7
```

##### Troubleshooting Routing

The easiest way to check routing on a small network is by pinging
between subnets.  As a rule, a ping will fail in the following ways:

* The ping will return a network unreachable message.  This will be returned when their is no route to the target.
* The ping will not receive a response.  This will be returned when the target packet cannot be returned -- either because there is no route from the target back to the source, or because packet forwarding is not turned on in the interim hosts.


##### Provisioning

We recommend ansible (https://www.ansible.com) to provision and manage
multiple hosts.  Ansible is an industry standard tool for remotely
provisioning and managing hosts which uses YML to define playbooks for
configuring assets.  Discussing the full configuration of ansible is
outside of the scope of this document, besides the Ansible community
already provides comprehensive documentation.  In this section, we
will discuss the common headaches involved in setting up ansible in
the testbed: authentication and defining the hosts.

##### Provisioning: Authentication

By default, Ansible ssh's into hosts using root.  As already seen with
all the sshing we have done previously, the testbed is configured so
users sudo into root.  To manage this, change the remote_user argument in
/etc/ansible/ansible.cfg to your userid. 

#### Provisioning: Hosts

You can dump the contents of the host query into the
/etc/ansible/hosts file and immediately use that with ansibles default
'all' keyword.

For example
````
ansible -m ping all
 [WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost
does not match 'all'
root@activexdc:/xp# mergetb show realization shorttermtopotest configtest -j | jq '.nodes | to_entries[].key' | tr -d '"' >> /etc/ansible/hosts
root@activexdc:/xp# ansible -m ping all

cr | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
inst_gw | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
inst | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
client | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
server_inst | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
server_uninst | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```



